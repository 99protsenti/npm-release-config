## [2.0.0](https://gitlab.com/99protsenti/npm-release-config/compare/v1.1.0...v2.0.0) (2024-12-02)

### ⚠ BREAKING CHANGES

* Drop compatibility with old conventional commits and node 18

### Features

* add support for node 22 ([47f0152](https://gitlab.com/99protsenti/npm-release-config/commit/47f0152fa098427bc1ea8e3de1c4f19ccfd3b6ed))
* update semantic-release dependencies ([9d120ab](https://gitlab.com/99protsenti/npm-release-config/commit/9d120abe33964329ca30b1f4fbc132c3723ee4ba))

## [1.1.0](https://gitlab.com/99protsenti/npm-release-config/compare/v1.0.0...v1.1.0) (2024-01-24)


### Features

* bump semantic release dependencies ([c349486](https://gitlab.com/99protsenti/npm-release-config/commit/c3494868ee48a868453d58e4d0e5d6c38f378490))

## 1.0.0 (2023-10-11)


### Features

* create releaserc configuration ([6eae357](https://gitlab.com/99protsenti/npm-release-config/commit/6eae357b875f1b83cce467a9db3ffc3aa9e4ee4c))
